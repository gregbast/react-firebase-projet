import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const app = firebase.initializeApp({
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: "react-firebase-redux-ed945.firebaseapp.com",
  projectId: "react-firebase-redux-ed945",
  storageBucket: "react-firebase-redux-ed945.appspot.com",
  messagingSenderId: "842396121368",
  appId: "1:842396121368:web:cb813af3eea1251dfd866d",
});

export const auth = app.auth();
export default app;
